﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElementaryCA
{
    //this class is basically just an abstraction of bool which will allow a cell to store more information
    //about itself in possible future updates
    class Cell
    {
        bool live;
        //getter/setter
        public bool Live
        {
            get
            {
                return live;
            }
            set
            {
                live = value;
            }
        }
        //constructor which sets the cells default state to dead
        public Cell()
        {
            live = false;
        }
    }
}
