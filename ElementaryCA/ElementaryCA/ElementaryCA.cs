using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

/* * 
 * An XNA visualization of the elementary one dimensional cellular automaton
 * this model uses the 8-bit rule indexing system first proposed by Stephen Wolfram
 * the actual calculation takes place in the Board class
 * since this project is not performance critical I have taken a novel or 'academic' approach to this
 * very interesting problem, it could certainly be done more efficiently, but this way is more fun :)
 * Author: Aaron Greig
 * Last updated: Feb 2016
 * */

namespace ElementaryCA
{
    public class ElementaryCA : Microsoft.Xna.Framework.Game
    {
        //Initializing values to be used later
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D rect;
        Board board;
        KeyboardState oldState = Keyboard.GetState();
        SpriteFont font;
        bool random = false;
        //This may seem like a strange choice of resolution but keeping it in 2:1 allows the rules to fit the screen very neatly
        int width = 1024;
        int height = 512;
        //setting the starting rule to a personal favourite
        int rule = 146;

        //the constructor called by my entry point
        public ElementaryCA()
        {
            //The intialization of the GraphicsDeviceManager must be done before anything else
            graphics = new GraphicsDeviceManager(this);
        }

        //The initialize override
        protected override void Initialize()
        {
            //initialize xna
            base.Initialize();
            //set the resolution
            graphics.PreferredBackBufferHeight = height;
            graphics.PreferredBackBufferWidth = width;
            //the app will only run in windowed mode
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
            //This is used by the XNA content pipeline to access resources
            Content.RootDirectory = "Content";
            //In this case my only resource is the font used to display which rule is on screen
            font = Content.Load<SpriteFont>("Font");
            //creating the initial instance of board with the default values I set up earlier
            board = new Board(rule, height, width, random);
            // initializing the spritebatch I will use for all my drawing
            spriteBatch = new SpriteBatch(GraphicsDevice);
            //The spritebatch object, as the name implies, can only draw sprites(or Texture2Ds in XNA terms), not primitives
            //rect is really just a 1x1 texture containing a single white pixel which I can use to visualize the cells on my board, white for live black for dead
            rect = new Texture2D(GraphicsDevice, 1, 1);
            rect.SetData(new[] { Color.White });
        }

        //the Update override
        protected override void Update(GameTime gameTime)
        {
            //This is where I handle input
            //the current state of the keyboard is obtained
            KeyboardState newState = Keyboard.GetState();

            //This looks a little strange at first glance but all it's saying is if the key is down this frame, and it wasn't down last frame
            //i.e it has just been pressed, react accordingly

            //use left and right keys to increment/decrement rule, allowing the user to effectively navigate all the possible rules
            if(newState.IsKeyDown(Keys.Right) && oldState.IsKeyUp(Keys.Right)){
                if(rule < 255){
                    rule++;
                    //every time the rule is changed a new instance of Board must be create with the new rule
                    board = new Board(rule, height, width, random);
                }
            }
            else if(newState.IsKeyDown(Keys.Left) && oldState.IsKeyUp(Keys.Left)){
                if(rule > 1){
                    rule--;
                    board = new Board(rule, height, width, random);
                }
            }
            //use tab to toggle random, a feature that is explained in the board class
            if (newState.IsKeyDown(Keys.Tab) && oldState.IsKeyUp(Keys.Tab))
            {
                random = !random;
                //like earlier, since a variable has changed the board must be recalculated and thus a new instance is created
                board = new Board(rule, height, width, random);
            }
            //allows the user to generate a new random board with R
            if (newState.IsKeyDown(Keys.R) && oldState.IsKeyUp(Keys.R) && random)
            {
                board = new Board(rule, height, width, random);
            }
            //exit with escape key
            if (newState.IsKeyDown(Keys.Escape) && oldState.IsKeyUp(Keys.Escape))
            {
                this.Exit();
            }

            //at the end of each update newState is stored as oldState, so as to access what was going on the last frame in the next update call
            oldState = newState;
            base.Update(gameTime);
        }

        //The Draw override
        protected override void Draw(GameTime gameTime)
        {
            //clear the screen
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            //iterate through the cells in board 
            //the nested loop is so that when I get to the draw I can simply use i and j as coordinates on screen
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    //if the cell we are on is alive, draw a pixel at its coordinate, otherwise leave it blank
                    if (board.boardArr[i, j].Live)
                    {
                        spriteBatch.Draw(rect, new Rectangle(j, i, 1, 1), Color.White);
                    }
                }
            }
            //show which rule is being displayed
            spriteBatch.DrawString(font, "Rule " + rule, new Vector2(25, 25), Color.Crimson);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
