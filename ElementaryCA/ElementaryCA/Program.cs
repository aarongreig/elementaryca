using System;

namespace ElementaryCA
{
#if WINDOWS || XBOX
    static class Program
    {
        //this is how XNA defines its entry point
        static void Main(string[] args)
        {
            using (ElementaryCA app = new ElementaryCA())
            {
                app.Run();
            }
        }
    }
#endif
}

