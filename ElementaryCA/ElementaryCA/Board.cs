﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElementaryCA
{
    //The Board class takes a rule in the form of an 8-bit number (an integer between 0 and 255)
    //and calculates an arbitrary number of generations (rows) of that rule, storing the resulting
    //grid of cells in an array which the xna part of the program uses to draw the images you see on screen
    class Board
    {
        //rows is how many generations will be calculated, columns is how wide each will be
        //in the theoritcal land of mathematics columns is infinite but unforunately my monitor is only 1600 pixels wide
        //so a limit must be defined
        int rows;
        int columns;
        //The array used to draw to the screen
        public Cell[,] boardArr;
        //This is where we will store what a cells state should be based on it and its naighbours state from the previous generation and the rule that has been given
        bool[] ruleStates = new bool[8];

        //This method calculates the states of each cell in the given row, based on the values in the previous row 
        void calcRow(int rowNo)
        {
            //for each cell in the row we will need to check the cells position in the previous row (i, rowNo-1), its neighbour to the left (i-1, rowNo-1)
            //and its neighbour to the right (i+1, rowNo-1)
            for (int i = 0; i < columns; i++)
            {
                //This string will be used to store the cells "neighbourhood", that is, what the values of it and the cells adjacent to it were in the last row
                //1 will represent a dead cell and 0 a live one, so if the cell was alive and both its neighbours were dead the result would be "101"
                //this seems like a backwards way of doing things, but it saves me from inverting the values later when I convert the string which represents
                //a binary number into an integer which I will use in conjunction with my ruleStates array to determine the state of the cell
                string nHood = "";
                //if we are on the left edge of the row
                if (i == 0)
                {
                    //if I tried to check i-1 here I would obviously get an error so I just assume that a cell which is out of bounds is a dead cell
                    nHood += "1";
                    //for the next two cells (we now know i and i+1 to be 0 and 1 respectively)
                    for (int j = 0; j < 2; j++)
                    {
                        //if the cell was alive then add a 0, otherwise add a 1
                        if (boardArr[rowNo-1, j].Live)
                        {
                            nHood += "0";
                        }
                        else
                        {
                            nHood += "1";
                        }
                    }
                }
                //if we are on the right edge of the row
                else if (i == columns - 1)
                {
                    //same idea as left edge but the dead cell gets added last since it is i+1 not i-1
                    for (int j = i - 1; j < columns; j++)
                    {
                        if (boardArr[rowNo-1, j].Live)
                        {
                            nHood += "0";
                        }
                        else
                        {
                            nHood += "1";
                        }
                    }
                    nHood += "1";
                }
                //for any other cell we can just run left to right from i-1 to i+1
                else
                {
                    for (int j = i - 1; j < i + 2; j++)
                    {
                        //again if the cell was alive add a 0, otherwise add a 1
                        if (boardArr[rowNo-1, j].Live)
                        {
                            nHood += "0";
                        }
                        else
                        {
                            nHood += "1";
                        }
                    }
                }
                //now that I have constructed my neighbourhood string out of zeroes and ones I can simply convert it
                //to an integer and use it as an index in my ruleStates array to determine whether the cell should be alive or dead
                //based on the rule that has been provided
                boardArr[rowNo, i].Live = ruleStates[Convert.ToInt32(nHood, 2)];
            }
        }

        //this method takes a rule (a number representable in eight bits) and breaks it down into an array of boolean values 
        //that represent the zeroes and ones that the number can be represented with
        void calcRule(int rule)
        {
            //first convert the integer into an array of characters which are either zero or one with one of C#s built in Convert functions
            char[] ruleStringArr = Convert.ToString(rule, 2).ToCharArray();
            //the convert function doesn't add any leading zeroes to make my character array up to the eight bits I need, which necessitates a bit of weirdness here
            //first I need a counter that I can increment independantly of the one I use for the loop
            int j = 0;
            //The value (8-the length of ruleStringArr) represents the maximum number of ones there can be in the rule
            //since the default value of boolean is false I will never have to carry out an operation on one of the leading zeroes
            //so by setting i based on the length of the string I can start my loop on the first 1, skipping the leading zeroes
            for (int i = 8 - ruleStringArr.Length; i < 8; i++)
            {
                if (ruleStringArr[j] == '1')
                {
                    ruleStates[i] = true;
                }
                else
                {
                    ruleStates[i] = false;
                }
                j++;
            }
        }

        //This method simply calls the calcRow method the required number of times
        void calcBoard()
        {
            //I can disregard the first row, or row 0, since it will be set in the constructor
            for (int i = 1; i < rows - 1; i++)
            {
                calcRow(i);
            }
        }

        //this method initializes the call array
        void populateBoard()
        {
            //define the size of the cell array
            boardArr = new Cell[rows, columns];
            //populate the cell array with cells
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    boardArr[i, j] = new Cell();
                }
            }
        }

        //The constructor
        public Board(int rule, int row, int column, bool random)
        {
            //set up the rule array with the given rule
            calcRule(rule);
            //set the rows and columns
            this.rows = row;
            this.columns = column;
            //set up the cell array
            populateBoard();
            //if the random parameter is true, populate the first row at random
            if (random)
            {
                Random rnd = new Random();
                for (int i = 0; i < columns; i++)
                {
                    //the default value of a cell is dead so for a random first row I just need to give each cell a 50% chance that it will be set to live
                    if (rnd.Next(2) == 1)
                    {
                        boardArr[0, i].Live = true;
                    }
                }
            }
            //if a random first row is not desired, only the cell in the very centre will be set to live
            //this is because many of rules present themselves very nicely as triangles evolving from a single point
            else
            {
                boardArr[0, columns / 2].Live = true;
            }
            //finally, calculate the rows
            calcBoard();
        }
    }
}
